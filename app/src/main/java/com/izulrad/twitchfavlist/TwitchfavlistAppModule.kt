package com.izulrad.twitchfavlist

import android.app.Application
import android.net.ConnectivityManager
import dagger.Module
import dagger.Provides
import org.jetbrains.anko.connectivityManager

@Module
class TwitchfavlistAppModule(val app: TwitchfavlistApp) {

    @Provides
    @ApplicationScope
    fun provideApplication(): Application = app

    @Provides
    @ApplicationScope
    fun provideConnectivityManager(app: Application): ConnectivityManager = app.connectivityManager

}