package com.izulrad.twitchfavlist.data.model.adapter

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.izulrad.twitchfavlist.data.model.Channel
import com.izulrad.twitchfavlist.data.model.Preview
import com.izulrad.twitchfavlist.data.model.Streamer
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.wtf

class StreamerAdapter : TypeAdapter<Streamer>(), AnkoLogger {

    override fun read(reader: JsonReader?): Streamer? {
        if (reader == null) return null

        val builder = Streamer.Builder()

        reader.beginObject()
        while (reader.hasNext()) {

            when (reader.nextName()) {
                "stream" -> {
                    if (reader.peek() != JsonToken.NULL) parseStream(reader, builder)
                    else reader.skipValue()
                }
                "_links" -> builder.username = parseLinksForName(reader)
                else -> reader.skipValue()
            }

        }
        reader.endObject()

        return builder.build()
    }

    private fun parseStream(reader: JsonReader, builder: Streamer.Builder) {
        reader.beginObject()
        while (reader.hasNext()) {

            when (reader.nextName()) {
                "game" -> builder.game = reader.nextString()
                "viewers" -> builder.viewers = reader.nextInt()
                "preview" -> builder.preview = parsePreview(reader)
                "channel" -> builder.channel = parseChannel(reader)
                else -> reader.skipValue()
            }

        }
        reader.endObject()
    }

    private fun parseLinksForName(reader: JsonReader): String {
        var username = ""

        reader.beginObject()
        while (reader.hasNext()) {
            if (reader.nextName() == "self") {
                username = Regex("[^/]*$").find(reader.nextString())!!.value
            } else reader.skipValue()
        }
        reader.endObject()

        return username
    }

    private fun parsePreview(reader: JsonReader): Preview {
        val builder = Preview.Builder()

        reader.beginObject()
        while (reader.hasNext()) {

            when (reader.nextName()) {
                "small" -> builder.small = reader.nextString()
                "medium" -> builder.medium = reader.nextString()
                "large" -> builder.large = reader.nextString()
                "template" -> builder.template = reader.nextString()
                else -> reader.skipValue()
            }

        }
        reader.endObject()

        return builder.build()
    }

    private fun parseChannel(reader: JsonReader): Channel {
        val builder = Channel.Builder()

        reader.beginObject()
        while (reader.hasNext()) {

            when (reader.nextName()) {
                "status" -> builder.status = reader.nextString()
                "game" -> builder.game = reader.nextString()
                "language" -> builder.language = reader.nextString()
                "logo" -> builder.logo = reader.nextString()
                "url" -> builder.url = reader.nextString()
                "url" -> builder.url = reader.nextString()
                "views" -> builder.views = reader.nextLong()
                "followers" -> builder.followers = reader.nextInt()
                else -> reader.skipValue()
            }

        }
        reader.endObject()

        return builder.build()
    }

    override fun write(out: JsonWriter?, value: Streamer?) {
        wtf("streamer adapter SHOULD NOT writing ever")
        throw UnsupportedOperationException()
    }

}