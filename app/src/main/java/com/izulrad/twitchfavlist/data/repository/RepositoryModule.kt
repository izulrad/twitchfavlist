package com.izulrad.twitchfavlist.data.repository

import android.app.Application
import android.content.SharedPreferences
import com.izulrad.twitchfavlist.ApplicationScope
import dagger.Module
import dagger.Provides
import org.jetbrains.anko.defaultSharedPreferences

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideSharedPreference(app: Application): SharedPreferences
        = app.defaultSharedPreferences

    @Provides
    @ApplicationScope
    fun provideFavStreamerNameRepo(sp: SharedPreferences): IFavStreamerNameRepo
        = FavStreamerNameRepoImp(sp)

}