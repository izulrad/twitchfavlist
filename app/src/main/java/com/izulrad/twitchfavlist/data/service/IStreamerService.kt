package com.izulrad.twitchfavlist.data.service

import com.izulrad.twitchfavlist.data.model.Streamer
import retrofit2.Call

interface IStreamerService {

    /* Возращает запросы всех стримеров из списка избранных (читать репорзитории) */
    fun findAllFavStreamers(): List<Call<Streamer>>

    /* Возвращает запрос со стримером по имени */
    fun findOne(name: String): Call<Streamer>

    /* Добавляет имя стримера в список избранных */
    fun addStreamerNameToFav(name: String)

    /* Удаляет имя стримера из списка избранных */
    fun removeStreamerNameFromFav(name: String)

}