package com.izulrad.twitchfavlist.data.service

import com.izulrad.twitchfavlist.ApplicationScope
import com.izulrad.twitchfavlist.data.repository.IFavStreamerNameRepo
import dagger.Module
import dagger.Provides
import retrofit2.GsonConverterFactory
import retrofit2.Retrofit

@Module
class ServiceModule {

    private val URL = "https://api.twitch.tv"
    private val NAME = "kraken"

    @Provides
    @ApplicationScope
    fun provideTwitchService(retrofit: Retrofit): ITwitchService
        = retrofit.create(ITwitchService::class.java)

    @Provides
    @ApplicationScope
    fun provideRetrofit(): Retrofit
        = Retrofit.Builder()
        .baseUrl("$URL/$NAME/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    @ApplicationScope
    fun provideStreamerService(twitchService: ITwitchService,
                               streamerNameRepoImp: IFavStreamerNameRepo): IStreamerService
        = StreamerServiceImp(twitchService, streamerNameRepoImp)

}