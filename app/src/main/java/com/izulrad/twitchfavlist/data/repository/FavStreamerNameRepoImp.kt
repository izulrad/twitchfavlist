package com.izulrad.twitchfavlist.data.repository

import android.content.SharedPreferences

class FavStreamerNameRepoImp(val mSharedPreferences: SharedPreferences) : IFavStreamerNameRepo {

    val STREAMERS_NAMES = "STREAMERS_NAMES"
    val DEFAULT_STREAMERS = setOf("igromania", "GarenaRU", "sheevergaming", "DethridgeCraft",
        "FerretBomb", "chinglishtv", "CohhCarnage", "TwoAngryGamersTV", "EddieRuckus", "Tesla",
        "Polovik")

    override fun saveStreamerName(name: String) {
        saveAllStreamerName(findAllStreamerNames() + name)
    }

    override fun removeStreamerName(name: String) {
        saveAllStreamerName(findAllStreamerNames() - name)
    }

    override fun saveAllStreamerName(names: Set<String>) {
        mSharedPreferences
            .edit()
            .putStringSet(STREAMERS_NAMES, names)
            .apply()
    }

    override fun findAllStreamerNames(): Set<String> {
        return  mSharedPreferences.getStringSet(STREAMERS_NAMES, DEFAULT_STREAMERS)
    }

}