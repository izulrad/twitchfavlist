package com.izulrad.twitchfavlist.data.service

import com.izulrad.twitchfavlist.data.model.Streamer
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ITwitchService {

    @GET("streams/{username}")
    fun findStreamer(@Path("username") username: String): Call<Streamer>

}