package com.izulrad.twitchfavlist.data.model

import com.google.gson.annotations.JsonAdapter
import com.izulrad.twitchfavlist.data.model.adapter.StreamerAdapter
import kotlin.properties.Delegates

@JsonAdapter(StreamerAdapter::class)
data class Streamer(
    val name: String,
    val game: String?=null,
    val viewers: Int? = null,
    val preview: Preview? = null,
    val channel: Channel? = null) {

    class Builder {
        var username: String by Delegates.notNull()
        var game: String? = null
        var viewers: Int? = null
        var preview: Preview? = null
        var channel: Channel? = null

        fun build() = Streamer(username, game, viewers, preview, channel)
    }
}
