package com.izulrad.twitchfavlist.data.model

import kotlin.properties.Delegates

data class Channel(
    val status: String,
    val game: String,
    val language: String,
    val logo: String?,
    val url: String,
    val views: Long,
    val followers: Int) {

    class Builder {
        var status: String by Delegates.notNull()
        var game: String  by Delegates.notNull()
        var language: String   by Delegates.notNull()
        var logo: String? = null
        var url: String  by Delegates.notNull()
        var views: Long  by Delegates.notNull()
        var followers: Int  by Delegates.notNull()

        fun build() = Channel(status, game, language, logo, url, views, followers)
    }
}