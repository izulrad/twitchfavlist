package com.izulrad.twitchfavlist.data.model

import kotlin.properties.Delegates

data class Preview(
    val small: String,
    val medium: String,
    val large: String,
    val template: String) {

    class Builder {
        var small: String by Delegates.notNull()
        var medium: String by Delegates.notNull()
        var large: String by Delegates.notNull()
        var template: String by Delegates.notNull()

        fun build() = Preview(small, medium, large, template)
    }
}
