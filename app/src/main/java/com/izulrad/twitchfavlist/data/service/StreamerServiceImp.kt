package com.izulrad.twitchfavlist.data.service

import com.izulrad.twitchfavlist.data.model.Streamer
import com.izulrad.twitchfavlist.data.repository.IFavStreamerNameRepo
import retrofit2.Call

class StreamerServiceImp(
    val twitchService: ITwitchService,
    val streamerRepo: IFavStreamerNameRepo
) : IStreamerService {

    override fun findAllFavStreamers(): List<Call<Streamer>> {
        return streamerRepo.findAllStreamerNames().map { twitchService.findStreamer(it) }
    }

    override fun findOne(name: String): Call<Streamer> {
        return twitchService.findStreamer(name)
    }

    override fun addStreamerNameToFav(name: String) {
        streamerRepo.saveStreamerName(name)
    }

    override fun removeStreamerNameFromFav(name: String) {
        streamerRepo.removeStreamerName(name)
    }

}