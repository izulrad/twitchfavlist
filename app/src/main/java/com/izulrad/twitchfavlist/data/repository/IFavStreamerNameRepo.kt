package com.izulrad.twitchfavlist.data.repository


interface IFavStreamerNameRepo {

    fun saveStreamerName(name: String)

    fun saveAllStreamerName(names: Set<String>)

    fun removeStreamerName(name: String)

    fun findAllStreamerNames(): Set<String>

}