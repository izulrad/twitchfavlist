package com.izulrad.twitchfavlist.ui.addfavstreamer

import com.izulrad.twitchfavlist.TwitchfavlistAppComponent
import com.izulrad.twitchfavlist.ui.addfavstreamer.view.AddStreamerToFavActivity
import dagger.Component

@AddFavStreamerScope
@Component(dependencies = arrayOf(TwitchfavlistAppComponent::class),
    modules = arrayOf(AddFavStreamerModule::class))
interface AddStreamerComponent {
    fun inject(activity: AddStreamerToFavActivity)
}