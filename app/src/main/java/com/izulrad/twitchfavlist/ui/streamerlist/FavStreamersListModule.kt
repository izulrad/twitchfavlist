package com.izulrad.twitchfavlist.ui.streamerlist

import android.net.ConnectivityManager
import com.izulrad.twitchfavlist.data.service.IStreamerService
import com.izulrad.twitchfavlist.ui.streamerlist.presenter.FavStreamersListPresenterImp
import com.izulrad.twitchfavlist.ui.streamerlist.presenter.IFavStreamersListPresenter
import com.izulrad.twitchfavlist.ui.streamerlist.view.FavStreamersListActivity
import com.izulrad.twitchfavlist.ui.streamerlist.view.StreamerListAdapter
import dagger.Module
import dagger.Provides

@Module
class FavStreamersListModule(
    val view: FavStreamersListActivity,
    val adapter: StreamerListAdapter
) {

    @Provides
    @FavStreamersListScope
    fun providePresenter(
        streamerService: IStreamerService,
        adapter: StreamerListAdapter,
        connectivityManager: ConnectivityManager
    ): IFavStreamersListPresenter
        = FavStreamersListPresenterImp(view, adapter, streamerService, connectivityManager)

    @Provides
    @FavStreamersListScope
    fun provideAdapter(): StreamerListAdapter = adapter

}