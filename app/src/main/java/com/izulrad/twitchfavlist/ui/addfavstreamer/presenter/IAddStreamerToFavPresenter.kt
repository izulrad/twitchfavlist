package com.izulrad.twitchfavlist.ui.addfavstreamer.presenter

interface IAddStreamerToFavPresenter {

    /* Добавить стримера в фавориты */
    fun addStreamerToFav(name: String)

}