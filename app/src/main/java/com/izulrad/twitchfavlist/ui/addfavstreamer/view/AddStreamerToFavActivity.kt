package com.izulrad.twitchfavlist.ui.addfavstreamer.view

import android.os.Bundle
import com.izulrad.twitchfavlist.R
import com.izulrad.twitchfavlist.TwitchfavlistAppComponent
import com.izulrad.twitchfavlist.ext.gone
import com.izulrad.twitchfavlist.ext.show
import com.izulrad.twitchfavlist.ext.trimString
import com.izulrad.twitchfavlist.ui.BaseActivity
import com.izulrad.twitchfavlist.ui.addfavstreamer.AddFavStreamerModule
import com.izulrad.twitchfavlist.ui.addfavstreamer.DaggerAddStreamerComponent
import com.izulrad.twitchfavlist.ui.addfavstreamer.presenter.IAddStreamerToFavPresenter
import kotlinx.android.synthetic.main.activity_add_streamer_to_fav_list.*
import org.jetbrains.anko.enabled
import org.jetbrains.anko.longToast
import javax.inject.Inject

class AddStreamerToFavActivity : BaseActivity(), IAddStreamerToFavView {

    @Inject
    lateinit var presenter: IAddStreamerToFavPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_streamer_to_fav_list)

        button_add_streamer_to_fav.setOnClickListener {
            presenter.addStreamerToFav(edit_text_streamer_name.trimString)
        }

    }

    override fun onComponentCreate(appComponent: TwitchfavlistAppComponent) {
        DaggerAddStreamerComponent
            .builder()
            .twitchfavlistAppComponent(appComponent)
            .addFavStreamerModule(AddFavStreamerModule(this))
            .build()
            .inject(this)
    }

    override fun preAddStreamer() {
        progress_bar_add_streamer_to_fav.show()
        button_add_streamer_to_fav.enabled = false
    }

    override fun postAddStreamerSuccess() {
        finish()
    }

    override fun postAddStreamerError(errorResId: Int) {
        progress_bar_add_streamer_to_fav.gone()
        edit_text_streamer_name.error = getString(errorResId)
        button_add_streamer_to_fav.enabled = true
    }

    override fun showError(resId: Int) {
        longToast(resId)
    }
}

