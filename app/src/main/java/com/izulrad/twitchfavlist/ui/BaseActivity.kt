package com.izulrad.twitchfavlist.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.izulrad.twitchfavlist.TwitchfavlistApp
import com.izulrad.twitchfavlist.TwitchfavlistAppComponent

abstract class BaseActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onComponentCreate(TwitchfavlistApp.get(applicationContext).component)
    }

    abstract fun onComponentCreate(appComponent: TwitchfavlistAppComponent)
}