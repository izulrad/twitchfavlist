package com.izulrad.twitchfavlist.ui.streamerlist

import com.izulrad.twitchfavlist.TwitchfavlistAppComponent
import com.izulrad.twitchfavlist.ui.streamerlist.view.FavStreamersListActivity
import dagger.Component

@FavStreamersListScope
@Component(dependencies = arrayOf(TwitchfavlistAppComponent::class),
    modules = arrayOf(FavStreamersListModule::class))
interface FavStreamersListComponent {
    fun inject(activity: FavStreamersListActivity)
}