package com.izulrad.twitchfavlist.ui.addfavstreamer.view

interface IAddStreamerToFavView {

    /* Выполняется перед попыткой добавлением стримера */
    fun preAddStreamer()

    /* Выполняется после успешной попытки добавления стримера */
    fun postAddStreamerSuccess()

    /* Выполняется после проваленной попытки добавления стримера */
    fun postAddStreamerError(errorResId: Int)

    /* На случай если что то пошло не так и об этом нужно сообщить юзеру*/
    fun showError(resId: Int)

}