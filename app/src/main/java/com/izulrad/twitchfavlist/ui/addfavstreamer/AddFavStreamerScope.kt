package com.izulrad.twitchfavlist.ui.addfavstreamer

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AddFavStreamerScope