package com.izulrad.twitchfavlist.ui.streamerlist.view

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.GridLayout
import android.widget.ImageView
import android.widget.TextView
import com.izulrad.twitchfavlist.R
import com.izulrad.twitchfavlist.data.model.Streamer
import org.jetbrains.anko.layoutInflater


class StreamerListAdapter(val ctx: Context) : ArrayAdapter<Streamer>(ctx, 0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val streamer: Streamer = getItem(position)

        val view = convertView ?:
            ctx.layoutInflater.inflate(R.layout.list_item_streamer_info, parent, false).apply{
                tag = ViewHolder(
                    findViewById(R.id.text_view_streamer_name) as TextView,
                    findViewById(R.id.image_view_online) as ImageView,
                    findViewById(R.id.image_view_offline)as ImageView,
                    findViewById(R.id.form_online) as GridLayout,
                    findViewById(R.id.text_view_game) as TextView,
                    findViewById(R.id.text_view_viewers) as TextView
                )
            }

        val holder = view.tag as ViewHolder

        holder.tvStreamerName.text = streamer.name
        if (streamer.viewers != null) {
            holder.imOnline.visibility = View.VISIBLE
            holder.imOffline.visibility = View.GONE
            holder.glFormOnline.visibility = View.VISIBLE
            holder.tvGame.text = streamer.game
            holder.tvViewers.text = "${streamer.viewers}"
        } else {
            holder.imOnline.visibility = View.GONE
            holder.imOffline.visibility = View.VISIBLE
            holder.glFormOnline.visibility = View.GONE
        }

        return view
    }

    private class ViewHolder(
        val tvStreamerName: TextView,
        val imOnline: ImageView,
        val imOffline: ImageView,
        val glFormOnline: GridLayout,
        val tvGame: TextView,
        val tvViewers: TextView
    )
}