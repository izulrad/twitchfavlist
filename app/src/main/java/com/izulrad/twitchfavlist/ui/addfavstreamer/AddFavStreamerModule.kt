package com.izulrad.twitchfavlist.ui.addfavstreamer

import android.net.ConnectivityManager
import com.izulrad.twitchfavlist.data.service.IStreamerService
import com.izulrad.twitchfavlist.ui.addfavstreamer.presenter.AddStreamerToFavPresenterImp
import com.izulrad.twitchfavlist.ui.addfavstreamer.presenter.IAddStreamerToFavPresenter
import com.izulrad.twitchfavlist.ui.addfavstreamer.view.AddStreamerToFavActivity
import dagger.Module
import dagger.Provides

@Module
class AddFavStreamerModule(val view: AddStreamerToFavActivity) {

    @Provides
    @AddFavStreamerScope
    fun providesPresenter(service: IStreamerService, connectivityManager: ConnectivityManager)
        : IAddStreamerToFavPresenter
        = AddStreamerToFavPresenterImp(view, service, connectivityManager)

}