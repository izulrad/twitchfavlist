package com.izulrad.twitchfavlist.ui.addfavstreamer.presenter

import android.net.ConnectivityManager
import com.izulrad.twitchfavlist.R
import com.izulrad.twitchfavlist.data.service.IStreamerService
import com.izulrad.twitchfavlist.ui.addfavstreamer.view.IAddStreamerToFavView
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

class AddStreamerToFavPresenterImp(
    val view: IAddStreamerToFavView,
    val service: IStreamerService,
    val cm: ConnectivityManager
) : IAddStreamerToFavPresenter {

    override fun addStreamerToFav(name: String) {
        if (cm.activeNetworkInfo?.isConnected != true) {
            view.showError(R.string.error_nas_no_internet)
            return
        }

        if (name.isEmpty()) {
            view.postAddStreamerError(R.string.error_streamer_name_cant_be_empty)
            return
        }

        view.preAddStreamer()

        async() {

            val streamerExist = service.findOne(name).execute().isSuccess

            uiThread {

                if (streamerExist) {
                    service.addStreamerNameToFav(name)
                    view.postAddStreamerSuccess()
                } else {
                    view.postAddStreamerError(R.string.error_streamer_name_not_available_or_exist)
                }
            }
        }
    }

}