package com.izulrad.twitchfavlist.ui.streamerlist.view

import android.os.Bundle
import android.support.v7.widget.SearchView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import com.izulrad.twitchfavlist.R
import com.izulrad.twitchfavlist.TwitchfavlistAppComponent
import com.izulrad.twitchfavlist.ext.hide
import com.izulrad.twitchfavlist.ext.show
import com.izulrad.twitchfavlist.ui.BaseActivity
import com.izulrad.twitchfavlist.ui.addfavstreamer.view.AddStreamerToFavActivity
import com.izulrad.twitchfavlist.ui.streamerlist.DaggerFavStreamersListComponent
import com.izulrad.twitchfavlist.ui.streamerlist.FavStreamersListModule
import com.izulrad.twitchfavlist.ui.streamerlist.presenter.IFavStreamersListPresenter
import kotlinx.android.synthetic.main.activity_fav_streamers_list.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class FavStreamersListActivity : BaseActivity(), IFavStreamersListView {

    @Inject
    lateinit var listPresenter: IFavStreamersListPresenter

    @Inject
    lateinit var adapter: StreamerListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fav_streamers_list)
        setSupportActionBar(toolbar_fav_streamers_list)

        registerForContextMenu(list_view_fav_streamers)

        list_view_fav_streamers.adapter = adapter

        search_view_by_streamer_name.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean = false

                override fun onQueryTextChange(newText: String?): Boolean {
                    listPresenter.setSiftName(newText ?: "")
                    return true
                }
            })
    }

    override fun onComponentCreate(appComponent: TwitchfavlistAppComponent) {
        DaggerFavStreamersListComponent
            .builder()
            .favStreamersListModule(FavStreamersListModule(this, StreamerListAdapter(this)))
            .twitchfavlistAppComponent(appComponent)
            .build()
            .inject(this)
    }

    override fun onResume() {
        super.onResume()
        listPresenter.refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fav_streamers_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_item_add_fav_streamer -> startActivity<AddStreamerToFavActivity>()

            R.id.menu_item_switch_visibility_offline_streamers ->
                listPresenter.switchVisibilityOfflineStreamers()
        }
        return false
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?,
                                     menuInfo: ContextMenu.ContextMenuInfo?) {
        menu?.add(R.string.action_remove_fav_streamer)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val pos = (item?.menuInfo as AdapterView.AdapterContextMenuInfo).position

        when (item?.title) {
            getString(R.string.action_remove_fav_streamer) ->
                listPresenter.removeStreamerFromFavListOnPos(pos)
        }

        return true
    }

    override fun preRefresh() {
        progress_bar_fav_streamers_list.show()
    }

    override fun postRefresh() {
        progress_bar_fav_streamers_list.hide()
    }

    override fun showError(resId: Int) {
        longToast(resId)
    }

}