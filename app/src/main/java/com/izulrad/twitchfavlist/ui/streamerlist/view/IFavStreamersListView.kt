package com.izulrad.twitchfavlist.ui.streamerlist.view


interface IFavStreamersListView {

    /* Выполняется перед обновлением данных
     * Можно включить прогрессбар например*/
    fun preRefresh()

    /* Выполняется после обновления данных */
    fun postRefresh()

    /* На случай если что то пошло не так и об этом нужно сообщить юзеру*/
    fun showError(resId: Int)

}