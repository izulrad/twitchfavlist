package com.izulrad.twitchfavlist.ui.streamerlist.presenter

import android.net.ConnectivityManager
import com.izulrad.twitchfavlist.R
import com.izulrad.twitchfavlist.data.model.Streamer
import com.izulrad.twitchfavlist.data.service.IStreamerService
import com.izulrad.twitchfavlist.ui.streamerlist.view.IFavStreamersListView
import com.izulrad.twitchfavlist.ui.streamerlist.view.StreamerListAdapter
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.async
import org.jetbrains.anko.error
import org.jetbrains.anko.uiThread
import java.util.*

class FavStreamersListPresenterImp(
    val view: IFavStreamersListView,
    val adapter: StreamerListAdapter,
    val service: IStreamerService,
    val cm: ConnectivityManager
) : AnkoLogger, IFavStreamersListPresenter {

    private val streamers = ArrayList<Streamer>()
    private var isShowOffline: Boolean = true
    private var siftName: String = ""

    override fun removeStreamerFromFavListOnPos(pos: Int) {
        val streamer = adapter.getItem(pos)
        service.removeStreamerNameFromFav(streamer.name)
        refresh()
    }

    override fun switchVisibilityOfflineStreamers() {
        isShowOffline = !isShowOffline
        refreshAdapter()
    }

    override fun setSiftName(siftName: String) {
        this.siftName = siftName
        refreshAdapter()
    }

    override fun refresh() {
        if (cm.activeNetworkInfo?.isConnected != true) {
            view.showError(R.string.error_nas_no_internet)
            return
        }

        view.preRefresh()
        streamers.clear()

        async() {

            val executes = service.findAllFavStreamers().map { it.execute() }

            uiThread {
                executes.forEach {
                    if (it.isSuccess) streamers.add(it.body())
                    else error(it.errorBody().string())
                }

                refreshAdapter()
                view.postRefresh()
            }
        }
    }

    private fun refreshAdapter() {
        adapter.clear()
        val filtered = streamers
            .filter { it.viewers != null || isShowOffline }
            .filter { it.name.toLowerCase().contains(siftName.toLowerCase()) }
        adapter.addAll(filtered)
    }
}