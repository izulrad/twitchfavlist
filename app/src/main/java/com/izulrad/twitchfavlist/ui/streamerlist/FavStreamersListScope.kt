package com.izulrad.twitchfavlist.ui.streamerlist

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FavStreamersListScope