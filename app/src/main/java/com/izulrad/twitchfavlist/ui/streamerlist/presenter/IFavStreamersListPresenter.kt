package com.izulrad.twitchfavlist.ui.streamerlist.presenter

interface IFavStreamersListPresenter {

    /* Обновить данные в списке стримеров */
    fun refresh()

    /* Показывает/скрывает оффлайн стримеров, в зависимости от текущего сотояния */
    fun switchVisibilityOfflineStreamers()

    /* Установить паттерн для просева
    * в списке будут отображаться только стримеры подходящие под паттерн, лайком */
    fun setSiftName(siftName: String)

    /* Удалить стримера с листа на переданной позиции*/
    fun removeStreamerFromFavListOnPos(pos: Int)

}