package com.izulrad.twitchfavlist

import android.app.Application
import android.content.Context


class TwitchfavlistApp : Application() {

    companion object {
        fun get(ctx: Context): TwitchfavlistApp = ctx.applicationContext as TwitchfavlistApp
    }

    lateinit var component: TwitchfavlistAppComponent
        private set

    override fun onCreate() {
        super.onCreate()

        component = DaggerTwitchfavlistAppComponent
            .builder()
            .twitchfavlistAppModule(TwitchfavlistAppModule(this))
            .build()
    }

}