package com.izulrad.twitchfavlist.ext

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

private val TAG = "!042 "

fun AnkoLogger.logi(message: Any?) {
    info(TAG + message)
}

fun AnkoLogger.loge(message: Any?){
    error(TAG + message)
}
