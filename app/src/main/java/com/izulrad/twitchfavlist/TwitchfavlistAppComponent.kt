package com.izulrad.twitchfavlist

import android.app.Application
import android.net.ConnectivityManager
import com.izulrad.twitchfavlist.data.repository.IFavStreamerNameRepo
import com.izulrad.twitchfavlist.data.repository.RepositoryModule
import com.izulrad.twitchfavlist.data.service.IStreamerService
import com.izulrad.twitchfavlist.data.service.ServiceModule
import dagger.Component

@ApplicationScope
@Component(modules = arrayOf(
    TwitchfavlistAppModule::class, RepositoryModule::class, ServiceModule::class
))
interface TwitchfavlistAppComponent {
    fun application(): Application
    fun favStreamerNameRepo(): IFavStreamerNameRepo
    fun streamerService(): IStreamerService
    fun connectivityManager(): ConnectivityManager
}